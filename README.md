INTRODUCTION
============
This is an extremely simple Web app that aggregates the various known [BMLT Root Servers](https://bmlt.magshare.net/installing-a-new-root-server/), and creates a "live" table that displays some basic statistics about those servers.

[It can be seen in action here.](https://bmlt.magshare.net/bmlt-tally)